/*
 * Go SMTP server library forked from GoGuerrilla and MailSlurper-Go
 *
 * Copyright (c) 2012 Flashmob, GuerrillaMail.com
 * Copyright (c) 2014 Keno Schwalb
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package smtp

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"time"
)

type Server struct {
	Addr         string
	Hostname     string
	HandlerFunc  HandlerFunc
	Sem          chan int // currently active clients
	MaxSize      int      // max email DATA size
	Timeout      time.Duration
	SaveMailChan chan *Client // workers for saving mail
}

type Client struct {
	State       int
	Helo        string
	MailFrom    string
	RcptTo      string
	Response    string
	Address     string
	Data        string
	Subject     string
	Hash        string
	Conn        net.Conn
	BufIn       *bufio.Reader
	BufOut      *bufio.Writer
	Time        int64
	KillTime    int64
	Errors      int
	ClientId    int64
	SavedNotify chan int
	MailItem    *MailItem
}

type MailItem struct {
	Id          int
	DateSent    string
	FromAddress string
	ToAddress   string
	Subject     string
	XMailer     string
	ContentType string
	Boundary    string
	Plain       string
	Html        string
	Attachments []*Attachment
}

type HandlerFunc func(*MailItem) error

func ListenAndServe(addr string, hostname string, handlerFunc HandlerFunc) error {
	server := &Server{Addr: addr, Hostname: hostname, HandlerFunc: handlerFunc}
	return server.ListenAndServe()
}

func (srv *Server) ListenAndServe() error {
	srv.Sem = make(chan int, 50)
	srv.SaveMailChan = make(chan *Client, 5)
	srv.Timeout = time.Duration(100)
	srv.MaxSize = 131072

	// start HandlerFunc workers
	go srv.HandleMail()

	// Start listening for SMTP connections
	listener, err := net.Listen("tcp", srv.Addr)
	if err != nil {
		return err
	}

	var clientId int64
	clientId = 1
	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
			/* fmt.Fprintf("Accept error: %s", err)
			continue */
		}

		srv.Sem <- 1 // Wait for active queue to drain.
		go srv.HandleClient(&Client{
			Conn:        conn,
			Address:     conn.RemoteAddr().String(),
			Time:        time.Now().Unix(),
			BufIn:       bufio.NewReader(conn),
			BufOut:      bufio.NewWriter(conn),
			ClientId:    clientId,
			SavedNotify: make(chan int),
		})
		clientId++
	}
}

func (srv *Server) HandleClient(client *Client) {
	defer srv.CloseClient(client)
	greeting := "220 " + srv.Hostname +
		" SMTP Guerrilla-SMTPd #" + strconv.FormatInt(client.ClientId, 10) + " (" + strconv.Itoa(len(srv.Sem)) + ") " + time.Now().Format(time.RFC1123Z)

	for i := 0; i < 100; i++ {
		switch client.State {
		case 0:
			srv.ResponseAdd(client, greeting)
			client.State = 1
		case 1:
			input, err := srv.ReadSmtp(client)
			if err != nil {
				// logln(1, fmt.Sprintf("Read error: %v", err))
				if err == io.EOF {
					// client closed the connection already
					return
				}
				if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
					// too slow, timeout
					return
				}
				break
			}
			input = strings.Trim(input, " \n\r")
			cmd := strings.ToUpper(input)
			switch {
			case strings.Index(cmd, "HELO") == 0:
				if len(input) > 5 {
					client.Helo = input[5:]
				}
				srv.ResponseAdd(client, "250 "+srv.Hostname+" Hello ")
			case strings.Index(cmd, "EHLO") == 0:
				if len(input) > 5 {
					client.Helo = input[5:]
				}
				srv.ResponseAdd(client, "250-"+srv.Hostname+" Hello "+client.Helo+"["+client.Address+"]"+"\r\n"+"250-SIZE "+strconv.Itoa(srv.MaxSize)+"\r\n250 HELP")
			case strings.Index(cmd, "MAIL FROM:") == 0:
				if len(input) > 10 {
					client.MailFrom = input[10:]
				}
				srv.ResponseAdd(client, "250 Ok")
			case strings.Index(cmd, "XCLIENT") == 0:
				// Nginx sends this
				// XCLIENT ADDR=212.96.64.216 NAME=[UNAVAILABLE]
				client.Address = input[13:]
				client.Address = client.Address[0:strings.Index(client.Address, " ")]
				fmt.Println("client address:[" + client.Address + "]")
				srv.ResponseAdd(client, "250 OK")
			case strings.Index(cmd, "RCPT TO:") == 0:
				if len(input) > 8 {
					client.RcptTo = input[8:]
				}
				srv.ResponseAdd(client, "250 Accepted")
			case strings.Index(cmd, "NOOP") == 0:
				srv.ResponseAdd(client, "250 OK")
			case strings.Index(cmd, "RSET") == 0:
				client.MailFrom = ""
				client.RcptTo = ""
				srv.ResponseAdd(client, "250 OK")
			case strings.Index(cmd, "DATA") == 0:
				srv.ResponseAdd(client, "354 Enter message, ending with \".\" on a line by itself")
				client.State = 2
			case strings.Index(cmd, "QUIT") == 0:
				srv.ResponseAdd(client, "221 Bye")
				srv.KillClient(client)
			default:
				srv.ResponseAdd(client, fmt.Sprintf("500 unrecognized command"))
				client.Errors++
				if client.Errors > 3 {
					srv.ResponseAdd(client, fmt.Sprintf("500 Too many unrecognized commands"))
					srv.KillClient(client)
				}
			}
		case 2:
			contents, err := srv.ReadSmtp(client)
			if err == nil {
				headers := &MailHeader{}
				headers.Parse(contents)

				body := &MailBody{}
				body.Parse(contents, headers.Boundary)

				var mailItem MailItem

				mailItem.Plain = body.TextBody
				if len(strings.TrimSpace(body.HTMLBody)) > 0 {
					mailItem.Html = body.HTMLBody
				}

				mailItem.ToAddress = client.RcptTo
				mailItem.FromAddress = headers.SenderName
				mailItem.Subject = headers.Subject
				mailItem.DateSent = headers.Date
				mailItem.XMailer = headers.XMailer
				mailItem.ContentType = headers.ContentType
				mailItem.Boundary = headers.Boundary
				mailItem.Attachments = body.Attachments

				client.MailItem = &mailItem

				// place on the channel so that one of the save mail workers can pick it up
				srv.SaveMailChan <- client
				// wait for the save to complete
				status := <-client.SavedNotify

				if status == 1 {
					srv.ResponseAdd(client, "250 OK : queued as "+client.Hash)
				} else {
					srv.ResponseAdd(client, "554 Error: transaction failed, blame it on the weather")
				}
			} else {
				//	logln(1, fmt.Sprintf("DATA read error: %v", err))
			}
			client.State = 1
		}

		// Send a response back to the client
		srv.ResponseWrite(client)
	}

}

func (srv *Server) ResponseAdd(client *Client, line string) {
	client.Response = line + "\r\n"
}
func (srv *Server) CloseClient(client *Client) {
	client.Conn.Close()
	<-srv.Sem // Done; enable next client to run.
}
func (srv *Server) KillClient(client *Client) {
	client.KillTime = time.Now().Unix()
}

func (srv *Server) ReadSmtp(client *Client) (input string, err error) {
	var reply string
	// Command state terminator by default
	suffix := "\r\n"
	if client.State == 2 {
		// DATA state
		suffix = "\r\n.\r\n"
	}
	for err == nil {
		client.Conn.SetDeadline(time.Now().Add(srv.Timeout * time.Second))
		reply, err = client.BufIn.ReadString('\n')
		if reply != "" {
			input = input + reply
			if len(input) > srv.MaxSize {
				err = errors.New("Maximum DATA size exceeded (" + strconv.Itoa(srv.MaxSize) + ")")
				return input, err
			}
		}
		if err != nil {
			break
		}
		if strings.HasSuffix(input, suffix) {
			break
		}
	}
	return input, err
}

func (srv *Server) ScanSubject(client *Client, reply string) {
	if client.Subject == "" && (len(reply) > 8) {
		test := strings.ToUpper(reply[0:9])
		if i := strings.Index(test, "SUBJECT: "); i == 0 {
			// first line with \r\n
			client.Subject = reply[9:]
		}
	} else if strings.HasSuffix(client.Subject, "\r\n") {
		// chop off the \r\n
		client.Subject = client.Subject[0 : len(client.Subject)-2]
		if (strings.HasPrefix(reply, " ")) || (strings.HasPrefix(reply, "\t")) {
			// subject is multi-line
			client.Subject = client.Subject + reply[1:]
		}
	}
}

func (srv *Server) ResponseWrite(client *Client) (err error) {
	var size int
	client.Conn.SetDeadline(time.Now().Add(srv.Timeout * time.Second))
	size, err = client.BufOut.WriteString(client.Response)
	client.BufOut.Flush()
	client.Response = client.Response[size:]
	return err
}

func (srv *Server) HandleMail() {
	for {
		client := <-srv.SaveMailChan
		err := srv.HandlerFunc(client.MailItem)
		if err != nil {
			client.SavedNotify <- -1
		} else {
			client.SavedNotify <- 1
		}
	}
}
