# SMTP
**smtp** is a simple smtp server library for [golang](http://golang.org)  
It is based on the server component of [GoGuerilla](https://github.com/flashmob/go-guerrilla) and the mail parser of [MailSlurper](https://github.com/flashmob/go-guerrilla).  
In contrast to most other server libraries, this one supports attachments.  

# Usage
The usage is similiar to net/http:

    smtp.ListenAndServe(interface string, hostname string, receiveMail smtp.HandlerFunc)

where **interface** is a string like `:25` or `127.0.0.1:2525`, **hostname** is the server's identification string and **receiveMail** is the function that is called for incoming emails.  
The function receiveMail is passed a `smtp.MailItem` and returns an error which is used to indicate successfull processing to the SMTP client  

This example should cover everything:

    package main

    import (
    	"0x.cx/caerostris/smtp"
    	"log"
    )

    func receiveMail(mailItem *smtp.MailItem) error {
    	log.Printf("Received mail: %s", mailItem.Body)

    	return nil
    }

    func main() {
    	err := smtp.ListenAndServe(":"+strconv.Itoa(port), "dutchman.vfl.pw", receiveMail)
    	if err != nil {
    		panic(err)
    	}
    }

Relevant structures:

    type MailItem struct {
    	DateSent    string
    	FromAddress string
    	ToAddress   string
    	Subject     string
    	XMailer     string
    	Body        string
    	ContentType string
    	Boundary    string
    	Attachments []*Attachment
    }

    type Attachment struct {
    	Headers  *AttachmentHeader
    	Contents string
    }

    type AttachmentHeader struct {
    	ContentType             string
    	Boundary                string
    	MIMEVersion             string
    	ContentTransferEncoding string
    	ContentDisposition      string
    	FileName                string
    	Body                    string
    }

# TODO

* Add SSL/TLS support
* Support multiple recipients

# License
Both GoGuerilla and MailSlurper are relaesed under the MIT license.
